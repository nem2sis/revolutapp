package com.marco.revolutapp.data.repository

import com.marco.revolutapp.config.RatesApplicationConfig
import com.marco.revolutapp.data.api.Api
import com.marco.revolutapp.data.api.CurrencyRatesForBaseResponse
import com.marco.revolutapp.data.entities.Rates
import com.marco.revolutapp.data.repository.remote.CurrencyRemoteRatesRestRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import org.junit.Test
import java.io.IOException
import java.util.*

class CurrencyRatesRestRepositoryTest {

    private val api = mockk<Api>()
    private val config = RatesApplicationConfig()

    private val currencyRatesRestRepository =
        CurrencyRemoteRatesRestRepository(
            api,
            config
        )

    @Test
    fun `given good API when running then returns correct list of rates`() {
        val base = "EUR"
        val response = CurrencyRatesForBaseResponse(
            base,
            emptyMap(),
            null
        )
        val model = Rates(
            Currency.getInstance(base),
            emptyMap()
        )
        every { api.getCurrencyRatesForBase(base) } returns Single.just(response)
        val test = currencyRatesRestRepository.observeCurrencyRatesForBase(base).test()
        verify { api.getCurrencyRatesForBase(base) }
        test
            .awaitCount(2)
            .assertNoErrors()
            .assertValueAt(0, model)
    }

    @Test
    fun `given incorrect API when running then returns an error`() {
        val base = "EUR"
        every { api.getCurrencyRatesForBase(base) } returns Single.error(IOException())
        val test = currencyRatesRestRepository.observeCurrencyRatesForBase(base).test()
        verify { api.getCurrencyRatesForBase(base) }
        test
            .awaitCount(2)
            .assertNoValues()
            .assertError(IOException::class.java)
    }


}