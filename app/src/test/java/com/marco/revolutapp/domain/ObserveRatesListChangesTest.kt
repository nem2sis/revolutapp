package com.marco.revolutapp.domain

import com.marco.revolutapp.config.RatesApplicationConfig
import com.marco.revolutapp.data.entities.Rates
import com.marco.revolutapp.data.entities.Selection
import com.marco.revolutapp.data.repository.local.CurrencyLocalPreferencesSelectionRepository
import com.marco.revolutapp.data.repository.remote.CurrencyRemoteRatesRestRepository
import com.marco.revolutapp.presentation.model.RowItemModel
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Flowable
import org.junit.Test
import java.math.BigDecimal
import java.math.MathContext
import java.util.*

class ObserveRatesListChangesTest {

    private val config = RatesApplicationConfig()
    private val repositoryRemote = mockk<CurrencyRemoteRatesRestRepository>()
    private val repositoryLocal = mockk<CurrencyLocalPreferencesSelectionRepository>()
    private val mathContext = MathContext(config.math_precision, config.math_rounding)

    private val observeRatesListChanges = ObserveRatesListChanges(
        repositoryRemote,
        repositoryLocal,
        config,
        mathContext
    )

    @Test
    fun `given base configuration of the repository when running then it should return a legit list of currencies`() {
        val base = Currency.getInstance("EUR")
        val amount = BigDecimal("100.0")
        every { repositoryRemote.observeCurrencyRatesForBase(base.currencyCode) } returns
                Flowable.just(
                    Rates(
                        base,
                        mapOf(Pair(Currency.getInstance("USD"), BigDecimal("12.0")))
                    )
                )
        every { repositoryLocal.observeSelectedCurrency() } returns
                Flowable.just(
                    Selection(
                        base, amount
                    )
                )
        val test = observeRatesListChanges.execute().test()
        verify { repositoryRemote.observeCurrencyRatesForBase(config.defaultBaseCurrency) }
        verify { repositoryLocal.observeSelectedCurrency() }
        test
            .awaitCount(1)
            .assertNoErrors()
            .assertValue(
                listOf(
                    RowItemModel(
                        base,
                        amount
                    ),
                    RowItemModel(
                        Currency.getInstance("USD"),
                        BigDecimal("12.00").times(config.defaultAmount)
                    )
                )
            )
    }

    @Test
    fun `given different selection when running it should return a list reflecting that change`() {
        val base = Currency.getInstance("EUR")
        val selection = Selection(Currency.getInstance("USD"), BigDecimal("300.0"))
        every { repositoryRemote.observeCurrencyRatesForBase(base.currencyCode) } returns
                Flowable.just(
                    Rates(
                        base,
                        mapOf(
                            Pair(
                                Currency.getInstance("GBP"),
                                BigDecimal("4.0")
                            ),
                            Pair(
                                Currency.getInstance("USD"),
                                BigDecimal("2.0")
                            )
                        )
                    )
                )
        every { repositoryLocal.observeSelectedCurrency() } returns
                Flowable.just(
                    selection
                )
        val test = observeRatesListChanges.execute().test()
        verify { repositoryRemote.observeCurrencyRatesForBase(config.defaultBaseCurrency) }
        verify { repositoryLocal.observeSelectedCurrency() }
        test
            .awaitCount(1)
            .assertNoErrors()
            .assertValue(
                listOf(
                    RowItemModel(
                        selection.currency,
                        selection.amount
                    ),
                    RowItemModel(
                        base,
                        BigDecimal("150")
                    ),
                    RowItemModel(
                        Currency.getInstance("GBP"),
                        BigDecimal("600.0")
                    )
                )
            )
    }
}