package com.marco.revolutapp.domain

import com.marco.revolutapp.config.RatesApplicationConfig
import com.marco.revolutapp.data.entities.Rates
import com.marco.revolutapp.data.entities.Selection
import com.marco.revolutapp.data.repository.local.CurrencyLocalSelectionRepository
import com.marco.revolutapp.data.repository.remote.CurrencyRemoteRatesRepository
import com.marco.revolutapp.presentation.model.RowItemModel
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import java.math.MathContext
import java.util.*
import javax.inject.Inject

class ObserveRatesListChanges @Inject constructor(
    private val repositoryRemote: CurrencyRemoteRatesRepository,
    private val repositoryLocal: CurrencyLocalSelectionRepository,
    private val config: RatesApplicationConfig,
    private val mathContext: MathContext
) {

    fun execute(): Flowable<List<RowItemModel>> {
        return Flowable.combineLatest(
            repositoryRemote.observeCurrencyRatesForBase(config.defaultBaseCurrency),
            repositoryLocal.observeSelectedCurrency(),
            BiFunction<Rates, Selection, List<RowItemModel>> { rates, selection ->
                rates.entries
                    .asSequence()
                    .map {
                        RowItemModel(
                            it.key,
                            calcValueFor(rates, selection, it.value)
                        )
                    }
                    .plus(
                        RowItemModel(
                            Currency.getInstance(config.defaultBaseCurrency),
                             calcValueFor(rates, selection, BigDecimal.ONE))
                    )
                    .sortedBy { it.currency.currencyCode }
                    .toList()
                    .sortedByDescending { it.currency.currencyCode == selection.currency.currencyCode }
            }
        )
            .subscribeOn(Schedulers.computation())
    }

    private fun calcValueFor(
        rates: Rates,
        selection: Selection,
        value: BigDecimal
    ): BigDecimal {
        return if (selection.currency.currencyCode.equals(
                config.defaultBaseCurrency,
                ignoreCase = true
            )
        ) {
            selection.amount.multiply(value, mathContext)
        } else {
            selection.amount.divide(rates.entries[selection.currency], mathContext)
                .multiply(value, mathContext)
        }
    }

}