package com.marco.revolutapp.domain

import com.marco.revolutapp.data.repository.local.CurrencyLocalSelectionRepository
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import javax.inject.Inject

class UpdateAmountUseCase @Inject constructor(private val repository: CurrencyLocalSelectionRepository) {

    fun execute(newAmount: BigDecimal): Completable {
        return repository.updateAmount(newAmount)
            .subscribeOn(Schedulers.io())
    }
}