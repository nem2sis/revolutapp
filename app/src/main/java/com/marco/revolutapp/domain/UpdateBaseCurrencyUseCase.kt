package com.marco.revolutapp.domain

import com.marco.revolutapp.data.repository.local.CurrencyLocalSelectionRepository
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class UpdateBaseCurrencyUseCase @Inject constructor(private val repository: CurrencyLocalSelectionRepository) {

    fun execute(newBase: Currency): Completable {
        return repository.updateBase(newBase)
            .subscribeOn(Schedulers.io())
    }
}