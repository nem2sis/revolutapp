package com.marco.revolutapp.presentation.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.marco.revolutapp.R
import com.marco.revolutapp.data.entities.HttpCallFailureException
import com.marco.revolutapp.data.entities.NoNetworkException
import com.marco.revolutapp.data.entities.ServerUnreachableException
import com.marco.revolutapp.presentation.model.RowItemModel
import com.marco.revolutapp.presentation.adapter.RatesAdapter
import kotlinx.android.synthetic.main.merge_rates_view.view.*

class RatesViewImpl @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet,
    defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle),
    RatesView {

    private lateinit var recyclerView: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var inputManager: InputMethodManager
    private lateinit var errorImage: ImageView

    override fun onFinishInflate() {
        super.onFinishInflate()
        View.inflate(context, R.layout.merge_rates_view, this)
        recyclerView = merge_rates_view_rv
        progressBar = merge_rates_view_pb
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if(newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    inputManager.hideSoftInputFromWindow(windowToken, 0)
                }
            }
        })
        progressBar.visibility = View.GONE
        errorImage = merge_rates_view_error_image
        showLoadingIndicator()
    }

    override fun setKeyboardReference(inputManager: InputMethodManager) {
        this.inputManager = inputManager
    }

    private fun hideLoadingIndicator() {
        progressBar.visibility = GONE
    }

    private fun showLoadingIndicator() {
        progressBar.visibility = View.VISIBLE
        recyclerView.visibility = View.GONE
    }

    override fun render(rates: List<RowItemModel>) {
        hideLoadingIndicator()
        recyclerView.visibility = View.VISIBLE
        recyclerView.adapter?.let {
            it as RatesAdapter
            it.updateWith(rates)
        }
        errorImage.visibility = View.GONE
    }

    override fun showErrorView(error: Exception, retryHandler: () -> Unit) {
        val message = when (error) {
            is NoNetworkException -> R.string.no_network_error
            is ServerUnreachableException -> R.string.unreachable_error
            is HttpCallFailureException -> R.string.call_error
            else -> R.string.generic_error
        }
        Snackbar.make(this, "Ops..${resources.getString(message)}", Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.rates_error_action) {
                retryHandler.invoke()
            }
            .setActionTextColor(resources.getColor(R.color.white))
            .show()
        hideLoadingIndicator()
        errorImage.visibility = View.VISIBLE
    }

    override fun loading() {
        errorImage.visibility = View.GONE
    }

    override fun setAdapter(adapter: RatesAdapter) {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            setAdapter(adapter)
            setHasFixedSize(true)
        }
    }
}