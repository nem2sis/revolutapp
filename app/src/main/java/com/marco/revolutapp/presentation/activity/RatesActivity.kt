package com.marco.revolutapp.presentation.activity

import android.os.Bundle
import android.util.Log
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.ViewModelProviders
import com.marco.revolutapp.R
import com.marco.revolutapp.di.RatesViewModelFactory
import com.marco.revolutapp.presentation.adapter.RatesAdapter
import com.marco.revolutapp.presentation.base.BaseActivity
import com.marco.revolutapp.presentation.model.RatesViewState
import com.marco.revolutapp.presentation.view.RatesView
import com.marco.revolutapp.presentation.view.RatesViewImpl
import com.marco.revolutapp.presentation.viewmodel.RatesViewModel
import io.reactivex.subscribers.ResourceSubscriber
import java.math.BigDecimal
import javax.inject.Inject

class RatesActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: RatesViewModelFactory
    @Inject
    lateinit var adapter: RatesAdapter
    @Inject
    lateinit var inputMethodManager: InputMethodManager
    private lateinit var viewModel: RatesViewModel
    private lateinit var view: RatesView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.rates_activity)
        view = findViewById<RatesViewImpl>(R.id.rates_activity_root_view)
        viewModel = ViewModelProviders.of(this, viewModelFactory)[RatesViewModel::class.java]
        view.setAdapter(adapter)
        view.setKeyboardReference(inputMethodManager)
    }

    override fun onResume() {
        super.onResume()
        observeRatesChanges()
        adapter.observeSelectionChanges()
            .subscribe {
                viewModel.onUpdateSelection(it)
            }
            .autoClear()
        adapter.observeAmountValueChanges()
            .subscribe {
                viewModel.onUpdateAmount(BigDecimal(it))
            }
            .autoClear()
    }

    private fun observeRatesChanges() {
        viewModel.observeChanges(
            object : ResourceSubscriber<RatesViewState>() {
                override fun onComplete() {
                    Log.e(TAG, "Complete")
                }

                override fun onError(t: Throwable?) {
                    Log.e(TAG, Log.getStackTraceString(t))
                    view.showErrorView(Exception(t)) {
                        handleRetry()
                    }
                }

                override fun onNext(state: RatesViewState?) {
                    when (state) {
                        is RatesViewState.Loading -> view.loading()
                        is RatesViewState.Render -> view.render(state.items)
                        is RatesViewState.Error -> {
                            view.showErrorView(state.error) {
                                handleRetry()
                            }
                        }
                    }
                }
            }
        )
    }

    private fun handleRetry() {
        observeRatesChanges()
    }

    companion object {
        private const val TAG = "RatesActivity"
    }
}