package com.marco.revolutapp.presentation.base

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {
    private val subscriptions: CompositeDisposable by lazy { CompositeDisposable() }

    protected fun Disposable.autoClear() {
        subscriptions.add(this)
    }

    public override fun onCleared() {
        subscriptions.dispose()
    }
}