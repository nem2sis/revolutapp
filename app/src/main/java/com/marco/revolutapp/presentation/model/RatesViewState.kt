package com.marco.revolutapp.presentation.model

sealed class RatesViewState {
    object Loading : RatesViewState()
    data class Render(val items: List<RowItemModel>): RatesViewState()
    data class Error(val error: Exception): RatesViewState()
}