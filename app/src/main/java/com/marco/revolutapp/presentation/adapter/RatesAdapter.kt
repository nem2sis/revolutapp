package com.marco.revolutapp.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.recyclerview.widget.RecyclerView
import coil.ImageLoader
import coil.api.load
import coil.transform.CircleCropTransformation
import com.jakewharton.rxbinding3.view.focusChanges
import com.jakewharton.rxbinding3.widget.textChanges
import com.marco.revolutapp.R
import com.marco.revolutapp.presentation.model.RowItemModel
import com.marco.revolutapp.utils.autoNotify
import com.marco.revolutapp.utils.focus
import com.marco.revolutapp.utils.generateCountryFlag
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.rates_list_item_view.view.*
import java.math.BigDecimal
import javax.inject.Inject
import kotlin.properties.Delegates


class RatesAdapter @Inject constructor(
    private val context: Context,
    private val imageLoader: ImageLoader,
    private val inputMethodManager: InputMethodManager
) : RecyclerView.Adapter<RatesAdapter.ItemViewHolder>() {

    init {
        setHasStableIds(true)
    }

    private var items: List<RowItemModel> by Delegates.observable(emptyList()) { _, oldList, newList ->
        autoNotify(oldList, newList)
    }

    private val amountChanges: PublishSubject<String> by lazy {
        PublishSubject.create<String>()
    }

    private val selectionChanges: PublishSubject<RowItemModel> by lazy {
        PublishSubject.create<RowItemModel>()
    }

    fun observeAmountValueChanges(): Observable<String> = amountChanges
    fun observeSelectionChanges(): Observable<RowItemModel> = selectionChanges

    override fun getItemCount(): Int {
        return items.size
    }

    fun updateWith(list: List<RowItemModel>) {
        items = list
    }

    override fun onBindViewHolder(
        holder: ItemViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        when (payloads.isEmpty()) {
            true -> onBindViewHolder(holder, position)
            false -> with(holder.currencyAmount) { if (!isFocused) setText((payloads[0] as BigDecimal).toPlainString()) }
        }
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = items[position]
        holder.currencyCode.text = item.currency.currencyCode
        holder.currencyName.text = item.currency.displayName
        holder.currencyAmount.let {
            if (!it.isFocused) {
                if (item.amount == BigDecimal.ZERO) {
                    it.setText("")
                } else {
                    it.setText(item.amount.toPlainString())
                }
            }
        }
        holder.currencyFlag.load(
            item.currency.generateCountryFlag(
                context, item.currency.currencyCode
            ), imageLoader
        ) {
            transformations(CircleCropTransformation())
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val viewHolder = ItemViewHolder(
            LayoutInflater.from(context).inflate(R.layout.rates_list_item_view, parent, false)
        )
        viewHolder.itemView.setOnClickListener {
            selectionChanges.onNext(items[viewHolder.adapterPosition])
            if (viewHolder.adapterPosition != 0) {
                viewHolder.currencyAmount.focus()
            }
        }

        viewHolder.currencyAmount.focusChanges()
            .skipInitialValue()
            .filter { it }
            .map { items[viewHolder.adapterPosition] }
            .doOnNext {
                inputMethodManager.showSoftInput(viewHolder.currencyAmount, 0)
            }
            .subscribe(selectionChanges)

        viewHolder.currencyAmount.textChanges()
            .skipInitialValue()
            .filter { viewHolder.currencyAmount.hasFocus() && it.isNotEmpty() }
            .map { it.toString() }
            .subscribe(amountChanges)


        return viewHolder
    }

    override fun getItemId(position: Int): Long = items[position].currency.hashCode().toLong()


    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val currencyCode = itemView.rates_list_item_code
        val currencyFlag = itemView.rates_list_item_flag
        val currencyAmount = itemView.rates_list_item_amount
        val currencyName = itemView.rates_list_item_name
    }
}

