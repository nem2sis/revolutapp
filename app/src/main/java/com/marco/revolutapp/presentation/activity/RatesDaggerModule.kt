package com.marco.revolutapp.presentation.activity

import android.app.Activity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.marco.revolutapp.di.RatesViewModelFactory
import com.marco.revolutapp.di.ViewModelKey
import com.marco.revolutapp.presentation.viewmodel.RatesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class RatesDaggerModule {

    @Binds
    abstract fun bindActivity(activity: RatesActivity): Activity

    @Binds
    abstract fun bindViewModelFactory(factory: RatesViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(RatesViewModel::class)
    abstract fun bindViewModel(viewModel: RatesViewModel): ViewModel
}