package com.marco.revolutapp.presentation.view

import android.view.inputmethod.InputMethodManager
import com.marco.revolutapp.presentation.model.RowItemModel
import com.marco.revolutapp.presentation.adapter.RatesAdapter

interface RatesView {

    fun render(rates: List<RowItemModel>)

    fun setKeyboardReference(inputManager: InputMethodManager)

    fun showErrorView(error: Exception, retryHandler: () -> Unit)

    fun loading()

    fun setAdapter(adapter: RatesAdapter)

}