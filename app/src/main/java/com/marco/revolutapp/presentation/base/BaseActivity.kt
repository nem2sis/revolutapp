package com.marco.revolutapp.presentation.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.android.AndroidInjection
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    private val subscriptions: CompositeDisposable by lazy { CompositeDisposable() }

    protected fun Disposable.autoClear() {
        subscriptions.add(this)
    }

    override fun onDestroy() {
        subscriptions.dispose()
        super.onDestroy()
    }
}