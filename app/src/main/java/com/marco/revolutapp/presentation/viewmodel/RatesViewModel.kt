package com.marco.revolutapp.presentation.viewmodel

import android.util.Log
import com.marco.revolutapp.domain.ObserveRatesListChanges
import com.marco.revolutapp.domain.UpdateAmountUseCase
import com.marco.revolutapp.domain.UpdateBaseCurrencyUseCase
import com.marco.revolutapp.presentation.model.RatesViewState
import com.marco.revolutapp.presentation.model.RowItemModel
import com.marco.revolutapp.presentation.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subscribers.ResourceSubscriber
import java.math.BigDecimal
import javax.inject.Inject

class RatesViewModel @Inject constructor(
    private val updateBaseCurrencyUseCase: UpdateBaseCurrencyUseCase,
    private val observeRatesListChanges: ObserveRatesListChanges,
    private val updateAmountUseCase: UpdateAmountUseCase
) : BaseViewModel() {

    fun observeChanges(subscriber: ResourceSubscriber<RatesViewState>) {
        subscriber.onNext(RatesViewState.Loading)
        observeRatesListChanges.execute()
            .map {
                RatesViewState.Render(
                    it
                )
            }
            .doOnError {
                RatesViewState.Error(
                    Exception(it)
                )
                Log.e("doOnError", it.message)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeWith(subscriber)
            .autoClear()
    }

    fun onUpdateAmount(newAmount: BigDecimal) {
        updateAmountUseCase.execute(newAmount)
            .subscribe()
            .autoClear()

    }

    fun onUpdateSelection(itemModel: RowItemModel) {
        updateBaseCurrencyUseCase.execute(itemModel.currency)
            .andThen(updateAmountUseCase.execute(itemModel.amount))
            .subscribe()
            .autoClear()
    }
}