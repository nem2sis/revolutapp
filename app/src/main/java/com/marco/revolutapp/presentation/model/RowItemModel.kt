package com.marco.revolutapp.presentation.model

import java.math.BigDecimal
import java.util.*

data class RowItemModel(val currency: Currency,
                        val amount: BigDecimal)