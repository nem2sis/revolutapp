package com.marco.revolutapp.data.entities

import java.math.BigDecimal
import java.util.*

data class Selection(val currency: Currency,
                     val amount: BigDecimal)