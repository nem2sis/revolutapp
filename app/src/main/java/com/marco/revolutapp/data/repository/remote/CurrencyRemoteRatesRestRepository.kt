package com.marco.revolutapp.data.repository.remote

import com.marco.revolutapp.config.RatesApplicationConfig
import com.marco.revolutapp.data.api.Api
import com.marco.revolutapp.data.entities.Rates
import com.marco.revolutapp.utils.mapNetworkErrors
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CurrencyRemoteRatesRestRepository @Inject constructor(
    private val api: Api,
    private val config: RatesApplicationConfig
) : CurrencyRemoteRatesRepository {

    override fun observeCurrencyRatesForBase(base: String): Flowable<Rates> {
        return api.getCurrencyRatesForBase(base)
            .compose(mapNetworkErrors())
            .map {
                Rates(
                    Currency.getInstance(it.base),
                    it.rates.map { map -> Currency.getInstance(map.key) to BigDecimal(map.value) }.toMap()
                )
            }
            .repeatWhen {
                it.delay(config.pollDelay, TimeUnit.MILLISECONDS)
            }
            .subscribeOn(Schedulers.io())
    }

}