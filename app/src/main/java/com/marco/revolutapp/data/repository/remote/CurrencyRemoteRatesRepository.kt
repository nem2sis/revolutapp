package com.marco.revolutapp.data.repository.remote

import com.marco.revolutapp.data.entities.Rates
import io.reactivex.Flowable

interface CurrencyRemoteRatesRepository {
    fun observeCurrencyRatesForBase(base: String) : Flowable<Rates>
}