package com.marco.revolutapp.data.repository.local

import com.f2prateek.rx.preferences2.Preference
import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.marco.revolutapp.config.RatesApplicationConfig
import com.marco.revolutapp.data.entities.Selection
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject

class CurrencyLocalPreferencesSelectionRepository @Inject constructor(
    sharedPreferences: RxSharedPreferences,
    config: RatesApplicationConfig
): CurrencyLocalSelectionRepository {

    private val BASE_CURRENCY_KEY = "base_currency_key"

    private val CURRENCY_AMOUNT_KEY = "currency_amount_key"

    override var baseCurrencyObservable: Preference<Currency> =
        sharedPreferences.getObject(
            BASE_CURRENCY_KEY,
            Currency.getInstance(config.defaultBaseCurrency),
            object : Preference.Converter<Currency> {
                override fun serialize(value: Currency): String {
                    return value.currencyCode
                }

                override fun deserialize(serialized: String): Currency {
                    return Currency.getInstance(serialized)
                }
            })

    override var selectedAmountObservable: Preference<BigDecimal> =
        sharedPreferences.getObject(CURRENCY_AMOUNT_KEY,
            config.defaultAmount,
            object : Preference.Converter<BigDecimal> {
                override fun deserialize(serialized: String): BigDecimal {
                    return BigDecimal(serialized)
                }

                override fun serialize(value: BigDecimal): String {
                    return value.toPlainString()
                }
            })

    override fun observeSelectedCurrency(): Flowable<Selection> {
        return Flowable.combineLatest(
            selectedAmountObservable.asObservable().toFlowable(BackpressureStrategy.LATEST),
            baseCurrencyObservable.asObservable().toFlowable(BackpressureStrategy.LATEST),
            BiFunction<BigDecimal, Currency, Selection> { amount, base ->
                Selection(base, amount)
            })
            .subscribeOn(Schedulers.computation())
    }

    override fun updateAmount(newAmount: BigDecimal): Completable {
        return Completable.fromAction {
            selectedAmountObservable.set(newAmount)
        }
    }

    override fun updateBase(newBase: Currency): Completable {
        return Completable.fromAction {
            baseCurrencyObservable.set(newBase)
        }
    }
}