package com.marco.revolutapp.data.api

import com.marco.revolutapp.BuildConfig
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    companion object {
        const val ENDPOINT = BuildConfig.REVOLUT_ENDPOINT
    }

    @GET("latest?")
    fun getCurrencyRatesForBase(@Query("base") base: String): Single<CurrencyRatesForBaseResponse>
}