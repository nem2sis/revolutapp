package com.marco.revolutapp.data.entities

import java.math.BigDecimal
import java.util.*

data class Rates(val base: Currency,
                 val entries: Map<Currency, BigDecimal>)