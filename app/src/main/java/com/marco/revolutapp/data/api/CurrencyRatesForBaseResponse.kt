package com.marco.revolutapp.data.api

import com.google.gson.annotations.SerializedName

data class CurrencyRatesForBaseResponse(
    @SerializedName("base") val base: String,
    @SerializedName("rates") val rates: Map<String, String>,
    @SerializedName("error") val error: String?
)