package com.marco.revolutapp.data.repository.local

import com.f2prateek.rx.preferences2.Preference
import com.marco.revolutapp.data.entities.Selection
import io.reactivex.Completable
import io.reactivex.Flowable
import java.math.BigDecimal
import java.util.*

interface CurrencyLocalSelectionRepository {

    var baseCurrencyObservable: Preference<Currency>

    var selectedAmountObservable: Preference<BigDecimal>

    fun observeSelectedCurrency(): Flowable<Selection>

    fun updateAmount(newAmount: BigDecimal): Completable

    fun updateBase(newBase: Currency): Completable
}