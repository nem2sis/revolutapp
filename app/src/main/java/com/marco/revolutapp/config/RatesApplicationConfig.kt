package com.marco.revolutapp.config

import java.math.BigDecimal
import java.math.RoundingMode

data class RatesApplicationConfig(
    val defaultBaseCurrency: String = "EUR",
    val defaultAmount: BigDecimal = BigDecimal(100.00),
    val pollDelay: Long = 1000L,
    val math_precision: Int = 6,
    val math_rounding: RoundingMode = RoundingMode.HALF_EVEN
)