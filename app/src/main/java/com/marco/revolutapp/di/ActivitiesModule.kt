package com.marco.revolutapp.di

import com.marco.revolutapp.presentation.activity.RatesActivity
import com.marco.revolutapp.presentation.activity.RatesDaggerModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesModule {

    // Activity subcomponents

    @ActivityScope
    @ContributesAndroidInjector(modules = [RatesDaggerModule::class])
    abstract fun contributeRatesActivity(): RatesActivity
}