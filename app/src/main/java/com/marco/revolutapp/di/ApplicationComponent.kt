package com.marco.revolutapp.di

import android.app.Application
import android.content.Context
import com.marco.revolutapp.RatesApplication
import com.marco.revolutapp.config.RatesApplicationConfig
import com.marco.revolutapp.presentation.activity.RatesDaggerModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        ActivitiesModule::class,
        AndroidInjectionModule::class,
        ApiModule::class,
        RepositoryModule::class,
        RatesDaggerModule::class
    ]
)
interface ApplicationComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun bindApplication(application: Application): Builder

        @BindsInstance
        fun bindContext(context: Context): Builder

        @BindsInstance
        fun bindConfig(configuration: RatesApplicationConfig): Builder

        fun build(): ApplicationComponent
    }

    fun inject(application: RatesApplication)
}