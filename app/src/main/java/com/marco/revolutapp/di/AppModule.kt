package com.marco.revolutapp.di

import android.content.Context
import android.content.SharedPreferences
import android.view.inputmethod.InputMethodManager
import coil.ImageLoader
import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.marco.revolutapp.config.RatesApplicationConfig
import dagger.Module
import dagger.Provides
import java.math.MathContext
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideMathContext(config: RatesApplicationConfig): MathContext {
        return MathContext(config.math_precision, config.math_rounding)
    }

    @Singleton
    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferences =
        context.getSharedPreferences("SharedPreferences", Context.MODE_PRIVATE)

    @Provides
    @Singleton
    fun provideRxSharedPreferences(sharedPreferences: SharedPreferences): RxSharedPreferences {
        return RxSharedPreferences.create(sharedPreferences)
    }

    @Singleton
    @Provides
    fun provideImageLoader(context: Context): ImageLoader {
        return ImageLoader(context) {
            availableMemoryPercentage(0.5)
            bitmapPoolPercentage(0.5)
            crossfade(true)
        }
    }

    @Singleton
    @Provides
    fun provideInputMethodManager(context: Context): InputMethodManager {
        return context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }
}
