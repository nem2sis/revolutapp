package com.marco.revolutapp.di

import com.f2prateek.rx.preferences2.RxSharedPreferences
import com.marco.revolutapp.config.RatesApplicationConfig
import com.marco.revolutapp.data.api.Api
import com.marco.revolutapp.data.repository.local.CurrencyLocalPreferencesSelectionRepository
import com.marco.revolutapp.data.repository.local.CurrencyLocalSelectionRepository
import com.marco.revolutapp.data.repository.remote.CurrencyRemoteRatesRepository
import com.marco.revolutapp.data.repository.remote.CurrencyRemoteRatesRestRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun provideCurrencyRatesRestRepository(
        api: Api,
        config: RatesApplicationConfig
    ): CurrencyRemoteRatesRepository {
        return CurrencyRemoteRatesRestRepository(
            api,
            config
        )
    }

    @Singleton
    @Provides
    fun provideCurrencySelectionLocalRepository(
        sharedPreferences: RxSharedPreferences,
        config: RatesApplicationConfig
    ): CurrencyLocalSelectionRepository {
        return CurrencyLocalPreferencesSelectionRepository(
            sharedPreferences,
            config
        )
    }
}