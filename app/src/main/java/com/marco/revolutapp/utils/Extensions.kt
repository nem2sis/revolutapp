package com.marco.revolutapp.utils

import android.content.Context
import android.view.View
import android.widget.EditText
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.DiffUtil
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import com.marco.revolutapp.R
import com.marco.revolutapp.data.entities.HttpCallFailureException
import com.marco.revolutapp.data.entities.NoNetworkException
import com.marco.revolutapp.data.entities.ServerUnreachableException
import com.marco.revolutapp.presentation.adapter.RatesAdapter
import com.marco.revolutapp.presentation.model.RowItemModel
import io.reactivex.Single
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.*

fun RatesAdapter.autoNotify(oldList: List<RowItemModel>, newList: List<RowItemModel>) {

    val diff = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition].currency.currencyCode == newList[newItemPosition].currency.currencyCode

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition].amount == newList[newItemPosition].amount

        override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? =
            newList[newItemPosition].amount

        override fun getOldListSize() = oldList.size
        override fun getNewListSize() = newList.size
    })
    diff.dispatchUpdatesTo(this)
}

fun EditText.focus() {
    requestFocus()
    setSelection(length())
}

fun Snackbar.show(view: View, message: String, listener: (view: View) -> Unit) {
    Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE)
        .setAction("Retry", listener)
        .show()
}

@DrawableRes
fun Currency.generateCountryFlag(context: Context, currencyCode: String): Int {
    val id =
        context.resources.getIdentifier(
            context.getString(R.string.drawable_flag_suffix).plus(
                currencyCode.toLowerCase(Locale.getDefault())
            ), context.getString(R.string.drawable_id_string),
            context.applicationInfo.packageName
        )
    return when (id) {
        0 -> R.drawable.flag__unknown
        else -> id
    }
}

fun <R> mapNetworkErrors() = { single: Single<R> ->
    single.onErrorResumeNext { error ->
        when (error) {
            is SocketTimeoutException -> Single.error(NoNetworkException(error))
            is UnknownHostException -> Single.error(ServerUnreachableException(error))
            is HttpException -> Single.error(HttpCallFailureException(error))
            else -> Single.error(error)
        }
    }
}